import { createRouter, createWebHistory } from "vue-router";

// Páginas
import Login from "@/views/auth/Login.vue";
import Acerca from "@/views/Acerca.vue";
import Dashboard from "@/views/dashboard/Dashboard.vue";
import Estudiantes from "@/views/listas/Estudiantes.vue";

// Layouts
import AuthLayout from "@/layouts/AuthLayout.vue";
import MainLayout from "@/layouts/MainLayout.vue";

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
    meta: { layout: AuthLayout },
  },
  {
    path: "/acerca",
    name: "Acerca",
    component: Acerca,
    meta: { layout: MainLayout },
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    meta: { layout: MainLayout },
  },
  {
    path: "/estudiantes",
    name: "Estudiantes",
    component: Estudiantes,
    meta: { layout: MainLayout },
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
