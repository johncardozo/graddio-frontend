import { createStore } from "vuex";

const store = createStore({
  state: {
    titulo: "",
    paginaActiva: "",
  },
  mutations: {
    setTitulo(state, titulo) {
      state.titulo = titulo;
    },
    setPaginaActiva(state, paginaActiva) {
      state.paginaActiva = paginaActiva;
    },
  },
});

export default store;
